import urllib, urllib2, json, webbrowser
import jinja2, os
from datetime import datetime

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None
 
def alchemyText(text, baseurl = 'https://gateway-a.watsonplatform.net/calls/text/TextGetEmotion',
    outputMode = 'json',
    params={},
    printURL = False
    ):
    params['apikey'] = 'd152373f28ff44a3bda04ab20d1a6c2fb9aa21d0'
    params['outputMode'] = outputMode
    params['text'] = text
    url = baseurl + "?" + urllib.urlencode(params)
    if printURL:
        print url
    return safeGet(url)
    

def makeDict(dict_2):
    dictionary = {}
    for emotion in dict_2['docEmotions'].keys():
        dictionary[emotion] = float(dict_2['docEmotions'][emotion])
    return dictionary
 

def getEmotions(text):      
    data = alchemyText(text)
    json_str = data.read()
    dict_2 = json.loads(json_str)
    logging.info(dict_2)
    emotions = makeDict(dict_2)
    return emotions
#
# Copyright 2010 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""A barebones AppEngine application that uses Facebook for login.

This application uses OAuth 2.0 directly rather than relying on Facebook's
JavaScript SDK for login. It also accesses the Facebook Graph API directly
rather than using the Python SDK. It is designed to illustrate how easy
it is to use the Facebook Platform without any third party code.

See the "appengine" directory for an example using the JavaScript SDK.
Using JavaScript is recommended if it is feasible for your application,
as it handles some complex authentication states that can only be detected
in client-side code.
"""

from secrets import FACEBOOK_APP_ID
from secrets import FACEBOOK_APP_SECRET

import base64
import cgi
import Cookie
import email.utils
import hashlib
import hmac
import logging
import time

import json
from google.appengine.ext import db
from google.appengine.ext.deferred import defer

#import jinja2
import webapp2
#, urllib, os

#import mydeferred

def hasASCII(post):
    for ch in post:
        if (ord(ch) < 128):
            return True
    return False

class User(db.Model):
    uid = db.StringProperty(required=True)
#    created = db.DateTimeProperty(auto_now_add=True)
#    updated = db.DateTimeProperty(auto_now=True)
    name = db.StringProperty(required=True)
    #profile_url = db.StringProperty(required=True)
    access_token = db.StringProperty(required=True)
#    location = db.StringProperty(required=False)

class Post():
    def __init__(self, post):
        self.message = post["message"].encode('ascii','ignore') 
        self.created_time_str = post["created_time"]
        self.created_time = Created(post["created_time"])
        self.emotions = getEmotions(post["message"].encode('ascii','ignore')) #dictionary, utf-8
        
## example created_time: "2016-05-18T22:40:09+0000"
    def __cmp__(self,other):
        selfPost = datetime.strptime(self.created_time_str, "%Y-%m-%dT%H:%M:%S+0000")
        otherPost = datetime.strptime(other.created_time_str, "%Y-%m-%dT%H:%M:%S+0000")
        return cmp(selfPost, otherPost)
    
class Created():
    def __init__(self, created_time):
        self.year = int(created_time[0:4].encode('utf-8'))
        self.month = int(created_time[5:7].encode('utf-8'))
        self.day = int(created_time[8:10].encode('utf-8'))
        self.time = CreatedTime(created_time[11:19].encode('utf-8'))
    
    def __str__(self):
        return "Created on %d.%d.%d"%(self.month ,self.day, self.year)
    
        
class CreatedTime():
    def __init__(self, created_time):
        self.hour = int(created_time[0:2])
        self.minute = int(created_time[3:5])
        self.sec = int(created_time[6:8])
        

def avgEmotions(posts):
    avgs = {}
    count = len(posts)
    for post in posts:
        for emo in post.emotions:
            if (avgs.has_key(emo)):
                avgs[emo] = avgs[emo] + post.emotions[emo]
            else:
                avgs[emo] = post.emotions[emo]
    for emo in avgs.keys():
        avgs[emo] = avgs[emo] / count
    return avgs

def hasEmotions(post):
    for emo in post.emotions.keys():
        if post.emotions[emo] != 0:
            return True
    return False
    
class BaseHandler(webapp2.RequestHandler):
    # @property followed by def current_user makes so that if x is an instance
    # of BaseHandler, x.current_user can be referred to, which has the effect of
    # invoking x.current_user()
    @property
    def current_user(self):
        """Returns the logged in Facebook user, or None if unconnected."""
        if not hasattr(self, "_current_user"):
            self._current_user = None
            # find the user_id in a cookie
            user_id = parse_cookie(self.request.cookies.get("fb_user"))
            if user_id:
                self._current_user = User.get_by_key_name(user_id)
        return self._current_user

class HomeHandler(BaseHandler):
    def get(self):
        logging.info("in home handler")
        #uses jinja templating with template oauth.html
        
        template = JINJA_ENVIRONMENT.get_template('index.html')
        args = {'current_user':self.current_user}
        
        if self.current_user is not None:
            
            access_token_ = self.current_user.access_token
            posts = json.load(urllib.urlopen(
                "https://graph.facebook.com/me/posts?" +
                urllib.urlencode(dict(access_token=access_token_))))
            logging.info(posts)
        
            post_data = posts['data']
            posts_list = [] 
            for post in post_data:
                #omit posts that do not have text or do not contain any ASCII
                if (post.has_key("message") and hasASCII(post['message'])):
                    post_obj = Post(post)
                    if (hasEmotions(post_obj)): #omit posts that do not have emotion values
                        posts_list.append(post_obj)
        
            sortedPosts = sorted(posts_list)
            logging.info(sortedPosts)
            
            args['avgs'] = avgEmotions(sortedPosts)
            args['posts']= sortedPosts
            
            if (len(sortedPosts) > 1):
                args['postlen'] = len(sortedPosts)    
        self.response.write(template.render(args))

# implemented by Eytan B and Paul R
# updated by SM
# gets all of the data from a group's feed, from FB, stores it in Datastore
# For deferred execution,
# definition of get_fbgroup_data has to be in a separate file for some reason
# I didn't see documentation of this on the AppEngine site, but see this discussion thread
# http://groups.google.com/group/google-appengine-python/browse_thread/thread/4621acb5244a0262/02f857286831faf7


class LoginHandler(BaseHandler):
    def get(self):
        args = dict(client_id=FACEBOOK_APP_ID)
        verification_code = self.request.get("code")
        if verification_code:
            # after successful login; redirected here with code;
            # use code to get the access_token from FB
            args["client_secret"] = FACEBOOK_APP_SECRET
            args["code"] = self.request.get("code")
            args['redirect_uri']=self.request.path_url
            ## for posts
            args['fields']='message,created_time'
            response = urllib.urlopen(
                "https://graph.facebook.com/oauth/access_token?" +
                urllib.urlencode(args))
            response_dict = cgi.parse_qs(response.read())
            logging.info(response_dict["access_token"])
            access_token_ = response_dict["access_token"][-1]

            # Download the Profile. Save it in Datastore; we'll need the
            # access_token later
            
            profile = json.load(urllib.urlopen(
                "https://graph.facebook.com/me?" +
                urllib.urlencode(dict(access_token=access_token_))))
            logging.info(profile)
            
            # list of posts
            
                    
            user = User(key_name=str(profile["id"]), uid=str(profile["id"]),
                        name=profile["name"], access_token=access_token_)       
                    
            #user = User(key_name=str(profile["id"]), uid=str(profile["id"]),
            #            name=profile["name"], access_token=access_token_,
            #            profile_url=profile["link"],
            #            location=profile.get('hometown', {'name':'unknown'})['name'])
                
            user.put()
            set_cookie(self.response, "fb_user", str(profile["id"]),
                       expires=time.time() + 30 * 86400)
            self.redirect("/")
        else:
            # not logged in yet-- send the user to FB to do that
            args['redirect_uri']=self.request.path_url
            #args["scope"] = "public_profile"
            args["scope"] = "user_posts"
            logging.info("https://graph.facebook.com/oauth/authorize?" +
                urllib.urlencode(args))
            self.redirect(
                "https://graph.facebook.com/oauth/authorize?" +
                urllib.urlencode(args))


class LogoutHandler(BaseHandler):
    def get(self):
        set_cookie(self.response, "fb_user", "", expires=time.time() - 86400)
        self.redirect("/")


def set_cookie(response, name, value, domain=None, path="/", expires=None):
    """Generates and signs a cookie for the give name/value"""
    timestamp = str(int(time.time()))
    value = base64.b64encode(value)
    signature = cookie_signature(value, timestamp)
    cookie = Cookie.BaseCookie()
    cookie[name] = "|".join([value, timestamp, signature])
    cookie[name]["path"] = path
    if domain: cookie[name]["domain"] = domain
    if expires:
        cookie[name]["expires"] = email.utils.formatdate(
            expires, localtime=False, usegmt=True)
    response.headers.add("Set-Cookie", cookie.output()[12:])


def parse_cookie(value):
    """Parses and verifies a cookie value from set_cookie"""
    if not value: return None
    parts = value.split("|")
    if len(parts) != 3: return None
    if cookie_signature(parts[0], parts[1]) != parts[2]:
        logging.warning("Invalid cookie signature %r", value)
        return None
    timestamp = int(parts[1])
    if timestamp < time.time() - 30 * 86400:
        logging.warning("Expired cookie %r", value)
        return None
    try:
        return base64.b64decode(parts[0]).strip()
    except:
        return None


def cookie_signature(*parts):
    """Generates a cookie signature.

    We use the Facebook app secret since it is different for every app (so
    people using this example don't accidentally all use the same secret).
    """
    chash = hmac.new(FACEBOOK_APP_SECRET, digestmod=hashlib.sha1)
    for part in parts: chash.update(part)
    return chash.hexdigest()


application = webapp2.WSGIApplication([(r"/", HomeHandler),(r"/auth/login", LoginHandler),(r"/auth/logout", LogoutHandler)], debug=True)

